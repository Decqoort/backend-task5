import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Task5 {

    public static void main(String[] args) {
        String pattern = "object-oriented programming";
        String replaceStr = "OOP";
        Scanner cin = new Scanner(System.in);
        String str = cin.nextLine();
        str = str.toLowerCase();
        StringBuilder builder = new StringBuilder(str);
        int count = 0;
        for (int i = 0; i < builder.length(); i++) {
            int index = builder.indexOf(pattern, i);
            i = index;
            if (index == -1) {
                break;
            }
            count ++;
            if (count % 2 == 0)
                builder = builder.replace(index, index + pattern.length(), replaceStr);
        }
        String answer = builder.toString();
        System.out.println("Строка с замененными словами: " + answer);
        String[] words = answer.split("\\W+");
        ArrayList<String> palindromes = new ArrayList<>();
        String minElem = null;
        int countOfLatin = 0;
        for (String elem: words) {
            if (IntStream.range(0, elem.length() / 2)
                    .noneMatch(i -> elem.charAt(i) != elem.charAt(elem.length() - i - 1)))
                palindromes.add(elem);
            if (elem.matches("^[a-zA-Z]+$"))
                countOfLatin ++;
            if (minElem == null)
                minElem = elem;
            else{
                if (elem.chars().distinct().count() < minElem.chars().distinct().count())
                    minElem = elem;
            }
        }

        System.out.println("Слово, состоящее из мин. кол-во различных символов: " + minElem);
        System.out.println("Кол-во слов, состоящее из латинских алфавита: " + countOfLatin);
        System.out.println("Слова палиндромы: ");
        for(String p: palindromes)
            System.out.println(p);
    }
}
